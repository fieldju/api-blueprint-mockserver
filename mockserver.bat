@echo off

::#####################################
:: Variables
::#####################################

SET BUILD_DIR=modules/apibms-core/build
SET LIB_DIR=%BUILD_DIR%/output/lib/*
SET MAIN_JAR=%BUILD_DIR%/libs/apibms-core.jar

::#####################################
:: Functions
::#####################################

:checkBuild
IF EXIST %MAIN_JAR% (
:: binaries already built
) ELSE (
echo.
echo Building MockServer from source
gradlew.bat clean copyDependencies
)
goto:eof

::#####################################
:: Start
::#####################################

:: determine if binaries are built
call:checkBuild

:: start the server
echo.
echo Starting MockServer
SET CLASSPATH=%BUILD_DIR%/libs/api-blueprint-mockserver-core.jar;%LIB_DIR

:: echo CLASSPATH=%CLASSPATH%

:: echo java -cp %CLASSPATH% com.gatehill.apibms.core.MockServerBootstrap
java -cp %CLASSPATH% com.gatehill.apibms.core.MockServerBootstrap %*
