package com.gatehill.apibms.core.service.requestmatcher;

import com.gatehill.apibms.core.exception.MethodNotAllowedException;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Select using all headers.
 */
public class AllHeadersRequestMatcher implements RequestMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllHeadersRequestMatcher.class);

    @Override
    public List<RequestDefinition> matchRequest(HttpServerExchange exchange, ResourceDefinition endpoint, List<RequestDefinition> candidates) throws MethodNotAllowedException {
        final List<RequestDefinition> matchedByHeaders = new ArrayList<>();

        for (RequestDefinition request : candidates) {
            if (allIncomingHeadersMatch(exchange, request)) {
                matchedByHeaders.add(request);
            }
        }

        LOGGER.trace("Matched {} candidate request definitions by headers for resource: {}", matchedByHeaders.size(), endpoint.getUrl());
        return matchedByHeaders;
    }

    /**
     * Check all mock headers are present in the incoming request. There may be additional incoming headers not in
     * the mock definition, but these are ignored.
     *
     * @param exchange
     * @param request
     * @return
     */
    private boolean allIncomingHeadersMatch(HttpServerExchange exchange, RequestDefinition request) {
        for (String headerName : request.getHeaders().keySet()) {
            final String mockValue = request.getHeaders().get(headerName);

            final HeaderValues requestValues = exchange.getRequestHeaders().get(headerName);
            if (requestValues.size() != 1 || !requestValues.get(0).equals(mockValue)) {
                LOGGER.trace("Failed to match header {} in request for path: {}", headerName, exchange.getRequestPath());
                return false;
            }
        }

        return true;
    }
}
