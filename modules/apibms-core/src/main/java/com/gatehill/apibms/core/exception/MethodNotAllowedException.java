package com.gatehill.apibms.core.exception;

/**
 * Created by pete on 19/02/2014.
 */
public class MethodNotAllowedException extends Exception {
    public MethodNotAllowedException(String message) {
        super(message);
    }
}
