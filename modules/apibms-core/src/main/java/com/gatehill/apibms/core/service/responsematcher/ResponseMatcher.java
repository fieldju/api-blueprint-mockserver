package com.gatehill.apibms.core.service.responsematcher;

import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import io.undertow.server.HttpServerExchange;

import java.util.Map;

/**
 * Filter mock responses that match the given {@link com.gatehill.apibms.core.model.RequestDefinition}.
 */
public interface ResponseMatcher {
    Map<Integer, ResponseDefinition> matchResponse(HttpServerExchange exchange, ResourceDefinition endpoint,
                                                   RequestDefinition request, Map<Integer, ResponseDefinition> candidates);
}
