package com.gatehill.apibms.core.service.requestmatcher;

import com.gatehill.apibms.core.exception.MethodNotAllowedException;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import io.undertow.server.HttpServerExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Match on verb.
 */
public class VerbRequestMatcher implements RequestMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(VerbRequestMatcher.class);

    @Override
    public List<RequestDefinition> matchRequest(HttpServerExchange exchange, ResourceDefinition endpoint, List<RequestDefinition> candidates) throws MethodNotAllowedException {
        final List<RequestDefinition> matchedByVerb = new ArrayList<>();

        // match on verb
        for (RequestDefinition request : candidates) {
            if (exchange.getRequestMethod().equalToString(request.getVerb())) {
                matchedByVerb.add(request);
            }
        }

        LOGGER.trace("Matched {} candidate request definitions by verb for resource: {}", matchedByVerb.size(), endpoint.getUrl());
        if (0 == matchedByVerb.size()) {
            throw new MethodNotAllowedException("Method " + exchange.getRequestMethod() + " not mapped for resource: " + endpoint.getUrl());
        }

        return matchedByVerb;
    }
}
