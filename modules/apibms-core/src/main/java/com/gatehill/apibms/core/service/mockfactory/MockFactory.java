package com.gatehill.apibms.core.service.mockfactory;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.MockDefinition;

import java.io.File;

/**
 * Created by pete on 15/02/2014.
 */
public interface MockFactory {
    public enum BlueprintFormat {
        MARKDOWN,
        AST_JSON,
        AST_YAML
    }

    /**
     * Build a {@link com.gatehill.apibms.core.model.MockDefinition} from an API Blueprint file.
     *
     * @param blueprintFile
     * @param format
     * @return
     * @throws ServiceException
     */
    MockDefinition createMock(File blueprintFile, BlueprintFormat format) throws ServiceException;
}
