package com.gatehill.apibms.core.model;

/**
 * Created by pete on 15/02/2014.
 */
public class MockServer {
    private int port;

    public MockServer(int port) {
        this.port = port;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
