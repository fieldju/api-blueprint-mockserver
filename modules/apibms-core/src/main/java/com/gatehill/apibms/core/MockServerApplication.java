package com.gatehill.apibms.core;

import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.service.ExecutionService;
import com.gatehill.apibms.core.service.ServerService;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: pete
 * Date: 23/02/2014
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
class MockServerApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockServerApplication.class);

    @Inject
    private ExecutionService executionService;

    @Inject
    private ServerService serverService;

    public int runApplication(File blueprintFile, MockFactory.BlueprintFormat format, Integer port) {
        // start server from blueprint
        final ExecutionInstance exec =
                executionService.execute(blueprintFile, format, port);

        LOGGER.info("Starting server on port {}", exec.getPort());

        // print endpoints
        final StringBuilder sbEndpoints = new StringBuilder();
        sbEndpoints.append("Endpoints:");
        for (ResourceDefinition endpoint : exec.getEndpoints()) {
            sbEndpoints.append(String.format("\r\n(%s) http://localhost:%s%s",
                    listRequestMethods(endpoint), exec.getPort(), endpoint.getUrl()));
        }
        LOGGER.info(sbEndpoints.toString());

        try {
            System.out.println("Press Enter to stop...");
            System.in.read();

        } catch (IOException e) {
            LOGGER.error("Error waiting for cancel");
        }

        // stop server
        System.out.println("Stopping...");
        serverService.stop(exec);

        return 0;
    }

    private String listRequestMethods(ResourceDefinition endpoint) {
        final StringBuilder sb = new StringBuilder();

        for (RequestDefinition request : endpoint.getRequests()) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(request.getVerb());
        }

        return sb.toString();
    }
}
