package com.gatehill.apibms.core.service.responsematcher;

import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.StatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Try to identify the most sensible default response, starting with 200 and working up,
 * then falling back to 100 and working up.
 */
public class DefaultResponseMatcher implements ResponseMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultResponseMatcher.class);

    @Override
    public Map<Integer, ResponseDefinition> matchResponse(HttpServerExchange exchange, ResourceDefinition endpoint, RequestDefinition request, Map<Integer, ResponseDefinition> candidates) {
        final Map<Integer, ResponseDefinition> matched = new HashMap<>();

        if (candidates.size() > 0) {
            // prefer 200
            if (candidates.containsKey(StatusCodes.OK)) {
                matched.put(StatusCodes.OK, candidates.get(StatusCodes.OK));
            }

            // find other response values in ascending order, starting at 2XX
            final List<Integer> orderedCodes = new ArrayList<>();
            orderedCodes.addAll(candidates.keySet());
            Collections.sort(orderedCodes);

            for (Integer statusCode : orderedCodes) {
                if (statusCode >= 200) {
                    matched.put(statusCode, candidates.get(statusCode));
                }
            }

            // still none found - check for 1XX codes
            for (Integer statusCode : orderedCodes) {
                if (statusCode < 200) {
                    matched.put(statusCode, candidates.get(statusCode));
                }
            }
        }

        LOGGER.trace("Matched {} candidate response definitions by default response code for resource: {}", matched.size(), endpoint.getUrl());
        return matched;
    }
}
