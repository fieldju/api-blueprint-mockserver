package com.gatehill.apibms.core.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by pete on 15/02/2014.
 */
public class ResourceDefinition {
    private String name;
    private String url;
    private List<RequestDefinition> requests = new ArrayList<>();
    private Map<Integer, ResponseDefinition> responses = new HashMap<>();

    public ResourceDefinition() {

    }

    public ResourceDefinition(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ResourceDefinition{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", requests=" + requests +
                ", responses=" + responses +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<RequestDefinition> getRequests() {
        return requests;
    }

    public void setRequests(List<RequestDefinition> requests) {
        this.requests = requests;
    }

    public Map<Integer, ResponseDefinition> getResponses() {
        return responses;
    }

    public void setResponses(Map<Integer, ResponseDefinition> responses) {
        this.responses = responses;
    }
}
