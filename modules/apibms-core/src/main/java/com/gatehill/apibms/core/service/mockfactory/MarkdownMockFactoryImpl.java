package com.gatehill.apibms.core.service.mockfactory;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import com.gatehill.apibms.core.service.ParsingService;
import io.undertow.util.Headers;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.inject.Inject;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;

/**
 * Build a {@link com.gatehill.apibms.core.model.MockDefinition} from a Markdown format API Blueprint file,
 * in pure Java (no external parser tool required).
 * <p/>
 * NOTE: This implementation is highly experimental and far from complete. Use it at your own risk.
 *
 * @see MockFactoryImpl
 */
public class MarkdownMockFactoryImpl implements MockFactory {
    private static final Logger LOGGER = LoggerFactory.getLogger(MarkdownMockFactoryImpl.class);

    @Inject
    private ParsingService parsingService;

    @Override
    public MockDefinition createMock(File blueprintFile, BlueprintFormat format) throws ServiceException {
        if (!BlueprintFormat.MARKDOWN.equals(format)) {
            throw new UnsupportedOperationException("This implementation only supports Markdown parsing");
        }

        LOGGER.info("Creating {} mock from file {}", format, blueprintFile);
        return createMock(parsingService.toXhtmlDocument(parsingService.parseMarkdownFile(blueprintFile)));
    }

    /**
     * Convert from MD format directly into a mock definition, without going via the AST.
     *
     * @param doc
     * @return
     * @throws ServiceException
     */
    private MockDefinition createMock(Document doc) throws ServiceException {
        try {
            final MockDefinition definition = new MockDefinition();

            // get body from document
            final XPath xPath = XPathFactory.newInstance().newXPath();

            final NodeList nodes = (NodeList) xPath.evaluate("/html/body",
                    doc.getDocumentElement(), XPathConstants.NODESET);

            if (nodes.getLength() != 1) {
                throw new ServiceException("XHTML should be exactly 1 body element");
            }

            final Node body = nodes.item(0);

            // build endpoint definitions
            if (body.hasChildNodes()) {
                ResourceDefinition currentEndpoint = null;

                for (int i = 0; i < body.getChildNodes().getLength(); i++) {
                    final Node child = body.getChildNodes().item(i);
                    LOGGER.debug("Found {}: {}", child.getNodeName(), child.getTextContent());

                    if (child.getNodeType() == Node.ELEMENT_NODE) {

                        // <h1>My API</h1>
                        if (isApiTitle(child.getNodeName())) {
                            definition.setName(child.getTextContent());
                            LOGGER.info("Found API: {}", definition.getName());
                        }

                        // <h2>GET /message</h2>
                        if (isEndpointHTTP(child.getNodeName())) {
                            // start of a new endpoint
                            currentEndpoint = new ResourceDefinition();
                            definition.addEndpoint(currentEndpoint);

                            LOGGER.info("Found endpoint: {}", currentEndpoint.getName());

                            final String[] http = child.getTextContent().split(" ");
                            currentEndpoint.setUrl(http[1]);

                            final RequestDefinition request = new RequestDefinition();
                            currentEndpoint.getRequests().add(request);
                            request.setVerb(http[0]);
                        }

                        // <ul><li><p>Response 200 (text/plain)</p>
                        if (isEndpointResponse(child.getNodeName())) {
                            final ResponseDefinition response = parseResponseDefinition(child);
                            populateResponseBody(response, child);
                            currentEndpoint.getResponses().put(response.getCode(), response);
                        }
                    }
                }
            }

            LOGGER.debug("Document parsed: {}", definition);
            return definition;

        } catch (XPathExpressionException e) {
            throw new ServiceException("Error creating mock from document: " + doc, e);
        }
    }

    /**
     * Get the response body.
     *
     * @param response
     * @param child
     * @throws XPathExpressionException
     */
    private void populateResponseBody(ResponseDefinition response, Node child) throws XPathExpressionException {
        LOGGER.debug("Parsing response definition]");

        // get body from document
        final XPath xPath = XPathFactory.newInstance().newXPath();

        final NodeList nodes = (NodeList) xPath.evaluate("li/pre/code", child, XPathConstants.NODESET);

        if (nodes.getLength() != 1) {
            throw new ServiceException("Response definition should have exactly 1 preformatted code element");
        }

        final Node code = nodes.item(0);

        final String textContent = code.getTextContent().trim();
        if (StringUtils.isNotBlank(textContent)) {
            response.setBody(textContent.trim());
        }
    }

    /**
     * Example:
     * <pre>
     *     <ul><li><p>Response 200 (text/plain)</p>
     * </pre>
     *
     * @param child
     * @return
     */
    private ResponseDefinition parseResponseDefinition(Node child) throws XPathExpressionException {
        LOGGER.debug("Parsing response definition]");

        // get body from document
        final XPath xPath = XPathFactory.newInstance().newXPath();

        final NodeList nodes = (NodeList) xPath.evaluate("li/p", child, XPathConstants.NODESET);

        if (nodes.getLength() != 1) {
            throw new ServiceException("Response definition should have exactly 1 paragraph element");
        }

        final Node p = nodes.item(0);

        // build definition
        final ResponseDefinition response = new ResponseDefinition();

        final String textContent = p.getTextContent().trim();
        final String[] responseCode = textContent.split(" ");
        if (!"Response".equals(responseCode[0])) {
            throw new ServiceException("Response definition should start with 'Response'");
        }

        // HTTP response code
        response.setCode(Integer.parseInt(responseCode[1]));

        // check for MIME type
        String restOfLine = textContent.substring(
                textContent.indexOf(response.getCode()) + responseCode[1].length());

        if (restOfLine.length() > 0) {
            restOfLine = restOfLine.trim();
            if (restOfLine.startsWith("(")) {
                restOfLine = restOfLine.substring(1);
            }
            if (restOfLine.endsWith(")")) {
                restOfLine = restOfLine.substring(0, restOfLine.length() - 1);
            }

            response.getHeaders().put(Headers.CONTENT_TYPE_STRING, restOfLine);
        }

        return response;
    }

    private boolean isEndpointResponse(String nodeName) {
        return "ul".equals(nodeName);
    }

    private boolean isEndpointHTTP(String nodeName) {
        return "h2".equals(nodeName);
    }

    private boolean isApiTitle(String nodeName) {
        return "h1".equals(nodeName);
    }
}
