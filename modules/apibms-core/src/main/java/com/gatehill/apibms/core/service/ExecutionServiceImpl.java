package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.MockServer;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;

import javax.inject.Inject;
import java.io.File;

/**
 * Created by pete on 18/02/2014.
 */
public class ExecutionServiceImpl implements ExecutionService {
    @Inject
    private MockFactory mockFactory;

    @Inject
    private ServerService serverService;

    @Override
    public ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format) throws ServiceException {
        return execute(blueprintFile, format, 0);
    }

    @Override
    public ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format, int port) throws ServiceException {
        final MockDefinition definition = mockFactory.createMock(blueprintFile, format);
        final MockServer server = serverService.start(definition, port);

        return new ExecutionInstance(server.getPort(),
                definition.getEndpoints().toArray(new ResourceDefinition[definition.getEndpoints().size()]));
    }
}
