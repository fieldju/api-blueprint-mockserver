package com.gatehill.apibms.core.model;

import java.util.Map;

/**
 * Created by pete on 19/02/2014.
 */
public class RequestDefinition {
    private String verb;
    private Map<String, String> headers;

    public String getVerb() {
        return verb;
    }

    public void setVerb(String verb) {
        this.verb = verb;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    @Override
    public String toString() {
        return "RequestDefinition{" +
                "verb='" + verb + '\'' +
                ", headers=" + headers +
                '}';
    }
}
