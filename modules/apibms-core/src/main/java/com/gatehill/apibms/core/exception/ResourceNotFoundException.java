package com.gatehill.apibms.core.exception;

/**
 * Created by pete on 19/02/2014.
 */
public class ResourceNotFoundException extends Exception {
    public ResourceNotFoundException(String message) {
        super(message);
    }
}
