package com.gatehill.apibms.core.model;

/**
 * Created with IntelliJ IDEA.
 * User: pete
 * Date: 23/02/2014
 * Time: 14:28
 * To change this template use File | Settings | File Templates.
 */
public class ExecutionInstance extends MockServer {
    private ResourceDefinition[] endpoints;

    public ExecutionInstance(int port, ResourceDefinition[] endpoints) {
        super(port);
        this.endpoints = endpoints;
    }

    public ResourceDefinition[] getEndpoints() {
        return endpoints;
    }
}
