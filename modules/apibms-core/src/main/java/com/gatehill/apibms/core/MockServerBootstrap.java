package com.gatehill.apibms.core;

import com.gatehill.apibms.core.service.mockfactory.MarkdownMockFactoryImpl;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.gatehill.apibms.core.service.mockfactory.MockFactoryImpl;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ExampleMode;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: pete
 * Date: 23/02/2014
 * Time: 11:34
 * To change this template use File | Settings | File Templates.
 */
public class MockServerBootstrap {
    private static final Logger LOGGER = LoggerFactory.getLogger(MockServerBootstrap.class);

    /**
     * Blueprint file.
     */
    @Option(name = "--blueprintFile", aliases = "-b", required = true, usage = "Blueprint file")
    public File blueprintFile;

    /**
     * Blueprint format.
     */
    @Option(name = "--blueprintFormat", aliases = "-f", usage = "Blueprint format")
    public MockFactory.BlueprintFormat format;

    /**
     * Blueprint format.
     */
    @Option(name = "--port", aliases = "-p", usage = "Port number")
    public Integer port;

    /**
     * Blueprint format.
     */
    @Option(name = "--pureJava", aliases = "-j", usage = "Pure Java Blueprint parser (experimental)")
    public boolean pureJava;

    public static void main(String... args) {
        final MockServerBootstrap bootstrap = new MockServerBootstrap();
        System.exit(bootstrap.runApplication(args));
    }

    private int runApplication(String[] args) {
        CmdLineParser cmdLineParser = new CmdLineParser(this);
        try {
            cmdLineParser.parseArgument(args);

        } catch (CmdLineException e) {
            // if there's a problem in the command line,
            // you'll get this exception. this will report
            // an error message.
            System.err.println(e.getMessage());
            System.err.println("./mockserver [options...]");

            // print the list of available options
            cmdLineParser.printUsage(System.err);
            System.err.println();

            // print option sample. This is useful some time
            System.err.println("  Example: ./mockserver"
                    + cmdLineParser.printExample(ExampleMode.ALL));

            return -1;
        }

        // infer format from filename
        if (null == format) {
            format = inferFormatFromFilename(blueprintFile);
            LOGGER.debug("No Blueprint format specified - inferred {} from Blueprint filename", format);
        }

        // if port not specified, default to 0 (= any free)
        if (null == port) {
            port = 0;
        }

        // get implementation
        final Class<? extends MockFactory> mockFactoryImplementation =
                (pureJava ? MarkdownMockFactoryImpl.class : MockFactoryImpl.class);

        // set up DI
        final Injector injector = Guice.createInjector(new BootstrapModule(format, mockFactoryImplementation));

        // bootstrap the application
        final MockServerApplication starter = injector.getInstance(MockServerApplication.class);
        starter.runApplication(blueprintFile, format, port);

        // normal exit
        return 0;
    }

    /**
     * Attempt to infer the format from the blueprint filename, defaulting to YAML
     * if no obvious match is found.
     *
     * @param astFile
     * @return
     */
    private MockFactory.BlueprintFormat inferFormatFromFilename(File astFile) {
        assert astFile != null;

        if (astFile.getAbsolutePath().toLowerCase().endsWith(".md")) {
            return MockFactory.BlueprintFormat.MARKDOWN;

        } else if (astFile.getAbsolutePath().toLowerCase().endsWith(".json")
                || astFile.getAbsolutePath().toLowerCase().endsWith(".js")) {

            return MockFactory.BlueprintFormat.AST_JSON;

        } else {
            return MockFactory.BlueprintFormat.AST_YAML;
        }
    }
}
