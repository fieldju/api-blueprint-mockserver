package com.gatehill.apibms.core.service.requestmatcher;

import com.gatehill.apibms.core.exception.MethodNotAllowedException;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import io.undertow.server.HttpServerExchange;

import java.util.List;

/**
 * Filter mock requests that match the given {@link io.undertow.server.HttpServerExchange}.
 */
public interface RequestMatcher {
    List<RequestDefinition> matchRequest(HttpServerExchange exchange, ResourceDefinition endpoint,
                                         List<RequestDefinition> candidates) throws MethodNotAllowedException;
}
