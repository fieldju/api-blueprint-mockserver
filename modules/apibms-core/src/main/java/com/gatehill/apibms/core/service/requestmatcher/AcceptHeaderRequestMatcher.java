package com.gatehill.apibms.core.service.requestmatcher;

import com.gatehill.apibms.core.exception.MethodNotAllowedException;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderValues;
import io.undertow.util.Headers;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Attempt selection using Accept header.
 */
public class AcceptHeaderRequestMatcher implements RequestMatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(AcceptHeaderRequestMatcher.class);

    @Override
    public List<RequestDefinition> matchRequest(HttpServerExchange exchange, ResourceDefinition endpoint, List<RequestDefinition> candidates) throws MethodNotAllowedException {
        final List<RequestDefinition> matchedByAcceptHeader = new ArrayList<>();

        final HeaderValues acceptHeader = exchange.getRequestHeaders().get(Headers.ACCEPT);
        if (null != acceptHeader && acceptHeader.size() > 0) {
            final String acceptValue = acceptHeader.get(0);
            if (StringUtils.isNotBlank(acceptValue)) {

                for (RequestDefinition request : candidates) {
                    final String mockRequestAccept = request.getHeaders().get(Headers.ACCEPT_STRING);
                    if (mockRequestAccept.equals(acceptValue)) {
                        matchedByAcceptHeader.add(request);
                    }
                }
            }
        }

        LOGGER.trace("Matched {} candidate request definitions by Accept header for resource: {}", matchedByAcceptHeader.size(), endpoint.getUrl());
        return matchedByAcceptHeader;
    }
}
