package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import org.w3c.dom.Document;

import java.io.File;
import java.io.InputStream;

/**
 * Created by pete on 15/02/2014.
 */
public interface ParsingService {
    /**
     * Convert a Markdown format file to HTML.
     *
     * @param markdownFile
     * @return
     * @throws ServiceException
     */
    String parseMarkdownFile(File markdownFile) throws ServiceException;

    /**
     * Convert a Markdown format {@link java.io.InputStream} to HTML.
     *
     * @param markdownStream
     * @return
     */
    String parseMarkdown(InputStream markdownStream);

    /**
     * Convert an HTML {@link String} to an XHTML {@link org.w3c.dom.Document}.
     *
     * @param html
     * @return
     * @throws ServiceException
     */
    Document toXhtmlDocument(String html) throws ServiceException;
}
