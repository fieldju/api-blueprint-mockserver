package com.gatehill.apibms.core.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pete on 15/02/2014.
 */
public class ResponseDefinition {
    private int code;
    private String body;
    private Map<String, String> headers = new HashMap<>();

    public void setCode(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "ResponseDefinition{" +
                "code=" + code +
                ", body='" + body + '\'' +
                ", headers=" + headers +
                '}';
    }
}
