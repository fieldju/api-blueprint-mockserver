package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;

import java.io.File;

/**
 * Created by pete on 18/02/2014.
 */
public interface ExecutionService {
    ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format)
            throws ServiceException;

    ExecutionInstance execute(File blueprintFile, MockFactory.BlueprintFormat format, int port)
            throws ServiceException;
}
