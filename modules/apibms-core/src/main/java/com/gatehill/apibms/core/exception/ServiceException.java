package com.gatehill.apibms.core.exception;

/**
 * Created by pete on 15/02/2014.
 */
public class ServiceException extends RuntimeException {
    public ServiceException(String message, Exception e) {
        super(message, e);
    }

    public ServiceException(String message) {
        super(message);
    }
}
