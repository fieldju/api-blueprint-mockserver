package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.model.MockServer;
import com.gatehill.apibms.core.model.MockDefinition;

/**
 * Created by pete on 15/02/2014.
 */
public interface ServerService {
    MockServer start(MockDefinition mock);

    MockServer start(MockDefinition mock, int port);

    void stop(MockServer server);
}
