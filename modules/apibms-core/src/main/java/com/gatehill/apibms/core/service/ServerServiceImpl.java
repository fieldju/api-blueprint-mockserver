package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.MethodNotAllowedException;
import com.gatehill.apibms.core.exception.ResourceNotFoundException;
import com.gatehill.apibms.core.exception.ServiceException;
import com.gatehill.apibms.core.model.*;
import com.gatehill.apibms.core.service.requestmatcher.AcceptHeaderRequestMatcher;
import com.gatehill.apibms.core.service.requestmatcher.AllHeadersRequestMatcher;
import com.gatehill.apibms.core.service.requestmatcher.RequestMatcher;
import com.gatehill.apibms.core.service.requestmatcher.VerbRequestMatcher;
import com.gatehill.apibms.core.service.responsematcher.ContentNegotationResponseMatcher;
import com.gatehill.apibms.core.service.responsematcher.DefaultResponseMatcher;
import com.gatehill.apibms.core.service.responsematcher.ResponseMatcher;
import io.undertow.Undertow;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.HeaderMap;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;
import io.undertow.util.StatusCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation using undertow.io to listen for requests and send a corresponding response back.
 */
public class ServerServiceImpl implements ServerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServerServiceImpl.class);

    /**
     * Sequentially executed request matchers.
     */
    private RequestMatcher[] requestMatchers = new RequestMatcher[]{
            new VerbRequestMatcher(),
            new AllHeadersRequestMatcher(),
            new AcceptHeaderRequestMatcher()
    };

    /**
     * Sequentially executed response matchers.
     */
    private ResponseMatcher[] responseMatchers = new ResponseMatcher[]{
            new ContentNegotationResponseMatcher(),
            new DefaultResponseMatcher()
    };

    /**
     * Running server instances.
     */
    final Map<Integer, Undertow> running = Collections.synchronizedMap(new HashMap<Integer, Undertow>());

    @Override
    public MockServer start(final MockDefinition mock) {
        try {
            return start(mock, getFreePort());

        } catch (IOException e) {
            throw new ServiceException("Error starting server for mock: " + mock, e);
        }
    }

    @Override
    public MockServer start(final MockDefinition mock, int port) {
        if (0 == port) {
            try {
                port = getFreePort();
                LOGGER.debug("No port specified - got free port {}", port);

            } catch (IOException e) {
                throw new ServiceException("Error getting free port", e);
            }
        }

        if (null != running.get(port)) {
            throw new ServiceException("Server already running on port " + port);
        }

        final Undertow server = Undertow.builder()
                .addHttpListener(port, "localhost")
                .setHandler(new HttpHandler() {
                    @Override
                    public void handleRequest(final HttpServerExchange exchange) throws Exception {
                        final ResourceDefinition endpoint = getEndpoint(mock, exchange);
                        if (null == endpoint) {
                            LOGGER.info("Sending HTTP {} for request: {}", StatusCodes.NOT_FOUND, exchange.getRequestPath());
                            exchange.setResponseCode(StatusCodes.NOT_FOUND);

                        } else {
                            // match request
                            final RequestDefinition request;
                            try {
                                request = matchRequest(exchange, endpoint);

                            } catch (ResourceNotFoundException e) {
                                LOGGER.info("Sending HTTP {} for request: {}", StatusCodes.NOT_FOUND, exchange.getRequestPath());
                                exchange.setResponseCode(StatusCodes.NOT_FOUND);
                                return;

                            } catch (MethodNotAllowedException e) {
                                LOGGER.info("Sending HTTP {} for request: {}", StatusCodes.METHOD_NOT_ALLOWED, exchange.getRequestPath());
                                exchange.setResponseCode(StatusCodes.METHOD_NOT_ALLOWED);
                                return;
                            }

                            // match response
                            final ResponseDefinition response = matchResponse(exchange, endpoint, request);

                            final int responseCode = response.getCode();
                            LOGGER.info("Sending HTTP {} for mock request: {}", responseCode,
                                    exchange.getRequestPath());

                            exchange.setResponseCode(responseCode);
                            populateResponseHeaders(response, exchange.getResponseHeaders());

                            // body
                            final String body = response.getBody();
                            exchange.getResponseHeaders().put(Headers.CONTENT_LENGTH, "" + (null != body ? body.length() : 0));
                            exchange.getResponseSender().send(body);
                        }
                    }
                }).build();
        server.start();
        LOGGER.info("Started mock server on port {} for mock {}", port, mock);

        // remember
        running.put(port, server);

        final MockServer mockServer = new MockServer(port);
        return mockServer;
    }

    /**
     * Apply matching logic to find the corresponding {@link com.gatehill.apibms.core.model.RequestDefinition}
     * for the incoming request.
     *
     * @param exchange
     * @param endpoint
     * @return
     * @throws MethodNotAllowedException
     */
    private RequestDefinition matchRequest(HttpServerExchange exchange, ResourceDefinition endpoint) throws MethodNotAllowedException, ResourceNotFoundException {
        List<RequestDefinition> candidates = endpoint.getRequests();

        for (RequestMatcher requestMatcher : requestMatchers) {
            LOGGER.trace("Trying {} to match request definition for resource {}",
                    requestMatcher.getClass().getName(), endpoint.getUrl());

            candidates = requestMatcher.matchRequest(exchange, endpoint, candidates);

            switch (candidates.size()) {
                case 0:
                    // stop looking if no requests found by matcher
                    break;

                case 1:
                    LOGGER.debug("Matched request definition {} using {} for resource {}",
                            candidates.get(0), requestMatcher.getClass().getName(), endpoint.getUrl());

                    return candidates.get(0);
            }
        }

        // 0 found after checking a matcher, or more than 1 found after checking all matchers
        throw new ResourceNotFoundException("Unable to find a single, distinct request definition matching incoming request for resource "
                + endpoint.getUrl());
    }

    /**
     * Apply matching logic to find the corresponding {@link com.gatehill.apibms.core.model.ResponseDefinition}
     * for the {@link com.gatehill.apibms.core.model.RequestDefinition}.
     *
     * @param exchange
     * @param endpoint
     * @param request
     * @return
     */
    private ResponseDefinition matchResponse(HttpServerExchange exchange, ResourceDefinition endpoint, RequestDefinition request) {
        for (ResponseMatcher responseMatcher : responseMatchers) {
            LOGGER.trace("Trying {} to match response definition for request {} to resource {}",
                    responseMatcher.getClass().getName(), request, endpoint.getUrl());

            final Map<Integer, ResponseDefinition> candidates = responseMatcher.matchResponse(exchange, endpoint, request, endpoint.getResponses());

            switch (candidates.size()) {
                case 0:
                    // don't stop looking if no response found by matcher
                    continue;

                case 1:
                    final ResponseDefinition response = candidates.values().iterator().next();
                    LOGGER.debug("Matched response definition {} using {} for request {} to resource {}",
                            response, responseMatcher.getClass().getName(), request, endpoint.getUrl());

                    return response;
            }
        }

        // 0 found after checking a matcher, or more than 1 found after checking all matchers
        throw new ServiceException("Unable to find a single, distinct response definition matching request for resource "
                + endpoint.getUrl());
    }

    /**
     * Populate the response headers from the {@link com.gatehill.apibms.core.model.ResponseDefinition}.
     *
     * @param response
     * @param responseHeaders
     */
    private void populateResponseHeaders(ResponseDefinition response, HeaderMap responseHeaders) {
        for (String headerName : response.getHeaders().keySet()) {
            responseHeaders.put(new HttpString(headerName), response.getHeaders().get(headerName));
        }

        if (!responseHeaders.contains(Headers.CONTENT_TYPE)) {
            LOGGER.trace("Adding default MIME type");
            responseHeaders.put(Headers.CONTENT_TYPE, "text/plain");
        }
    }

    /**
     * Get endpoint for the request path.
     *
     * @param mock
     * @param exchange
     * @return
     */
    private ResourceDefinition getEndpoint(MockDefinition mock, HttpServerExchange exchange) {
        for (ResourceDefinition endpoint : mock.getEndpoints()) {
            if (endpoint.getUrl().equals(exchange.getRequestPath())) {
                return endpoint;
            }
        }

        if (!shouldIgnoreMissingPath(exchange.getRequestPath())) {
            throw new ServiceException("Unknown endpoint path: " + exchange.getRequestPath());
        } else {
            return null;
        }
    }

    private boolean shouldIgnoreMissingPath(String requestPath) {
        switch (requestPath) {
            case "/favicon.ico":
                return true;
        }
        return false;
    }

    private int getFreePort() throws IOException {
        ServerSocket s = new ServerSocket(0);
        final int port = s.getLocalPort();
        s.close();
        return port;
    }

    @Override
    public void stop(MockServer server) {
        final Undertow u = running.get(server.getPort());
        if (null != u) {
            LOGGER.info("Stopping server on port {}", server.getPort());

            u.stop();
            running.remove(server.getPort());

        } else {
            LOGGER.warn("No server to stop on port {}", server.getPort());
        }
    }

    public void setRequestMatchers(RequestMatcher[] requestMatchers) {
        this.requestMatchers = requestMatchers;
    }

    public void setResponseMatchers(ResponseMatcher[] responseMatchers) {
        this.responseMatchers = responseMatchers;
    }
}
