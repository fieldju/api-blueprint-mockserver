package com.gatehill.apibms.core.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pete on 15/02/2014.
 */
public class MockDefinition {
    private List<ResourceDefinition> endpoints = new ArrayList<>();
    private String name;

    public void addEndpoint(ResourceDefinition endpoint) {
        endpoints.add(endpoint);
    }

    public List<ResourceDefinition> getEndpoints() {
        return endpoints;
    }

    @Override
    public String toString() {
        return "MockDefinition{" +
                "endpoints=" + endpoints +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
