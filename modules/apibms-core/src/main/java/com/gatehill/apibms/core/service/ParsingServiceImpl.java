package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.exception.ServiceException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.markdown4j.Markdown4jProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by pete on 15/02/2014.
 */
public class ParsingServiceImpl implements ParsingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ParsingServiceImpl.class);

    @Override
    public String parseMarkdownFile(File markdownFile) throws ServiceException {
        try {
            LOGGER.debug("Parsing markdown file: {}", markdownFile);
            return parseMarkdown(FileUtils.openInputStream(markdownFile));

        } catch (IOException e) {
            throw new ServiceException("Error parsing markdown file: " + markdownFile, e);
        }
    }

    @Override
    public String parseMarkdown(InputStream markdownStream) throws ServiceException {
        try {
            // convert MD to HTML
            LOGGER.debug("Converting markdown InputStream to String");
            return new Markdown4jProcessor().process(markdownStream);

        } catch (IOException e) {
            throw new ServiceException("Error parsing markdown stream", e);
        }
    }

    @Override
    public Document toXhtmlDocument(String html) throws ServiceException {
        try {
            LOGGER.debug("Converting HTML to XHTML document");
            LOGGER.trace("HTML: {}", html);

            // convert HTML to X(HT)ML
            final Tidy tidy = new Tidy(); // obtain a new Tidy instance
            tidy.setXHTML(true); // set desired config options using tidy setters

            final Document xhtml;
            try (InputStream inputStream = IOUtils.toInputStream(html)) {
                xhtml = tidy.parseDOM(inputStream, System.out);
            }

            return xhtml;

        } catch (IOException e) {
            throw new ServiceException("Error converting HTML to XHTML document", e);
        }
    }
}
