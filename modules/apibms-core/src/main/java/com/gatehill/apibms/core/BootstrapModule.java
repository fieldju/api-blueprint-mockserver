package com.gatehill.apibms.core;

import com.gatehill.apib.parser.service.*;
import com.gatehill.apibms.core.service.ExecutionService;
import com.gatehill.apibms.core.service.ExecutionServiceImpl;
import com.gatehill.apibms.core.service.ServerService;
import com.gatehill.apibms.core.service.ServerServiceImpl;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.google.inject.Binder;
import com.google.inject.Module;
import com.google.inject.Singleton;

/**
 * Created with IntelliJ IDEA.
 * User: pete
 * Date: 23/02/2014
 * Time: 11:36
 * To change this template use File | Settings | File Templates.
 */
public class BootstrapModule implements Module {
    private final Class<? extends MockFactory> mockFactoryClass;
    private final MockFactory.BlueprintFormat format;

    public BootstrapModule(MockFactory.BlueprintFormat format, Class<? extends MockFactory> mockFactoryClass) {
        assert format != null;
        this.format = format;

        assert mockFactoryClass != null;
        this.mockFactoryClass = mockFactoryClass;
    }

    @Override
    public void configure(Binder binder) {
        binder.bind(MockFactory.class).to(mockFactoryClass).in(Singleton.class);
        binder.bind(ServerService.class).to(ServerServiceImpl.class).in(Singleton.class);
        binder.bind(ExecutionService.class).to(ExecutionServiceImpl.class).in(Singleton.class);

        // parsers
        binder.bind(BlueprintParserService.class).to(SnowcrashBlueprintParserServiceImpl.class);

        switch (format) {
            case MARKDOWN:
                // MD gets converted to JSON
            case AST_JSON:
                binder.bind(AstParserService.class).to(JsonAstParserServiceImpl.class);
                break;

            case AST_YAML:
                binder.bind(AstParserService.class).to(YamlAstParserServiceImpl.class);
                break;

            default:
                throw new UnsupportedOperationException("Unsupported format: " + format);
        }
    }
}
