package com.gatehill.apibms.core.integration;

import com.gatehill.apib.parser.service.AstParserService;
import com.gatehill.apib.parser.service.BlueprintParserService;
import com.gatehill.apib.parser.service.JsonAstParserServiceImpl;
import com.gatehill.apib.parser.service.SnowcrashBlueprintParserServiceImpl;
import com.gatehill.apibms.core.model.ExecutionInstance;
import com.gatehill.apibms.core.service.ExecutionService;
import com.gatehill.apibms.core.service.ExecutionServiceImpl;
import com.gatehill.apibms.core.service.ServerService;
import com.gatehill.apibms.core.service.ServerServiceImpl;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import com.gatehill.apibms.core.service.mockfactory.MockFactoryImpl;
import com.google.inject.*;
import com.jayway.restassured.RestAssured;
import io.undertow.util.StatusCodes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * Created by pete on 15/02/2014.
 */
public class EndToEndTest {

    private ExecutionService service;
    private ServerService serverService;
    private Injector injector;

    @Before
    public void before() {
        service = new ExecutionServiceImpl();

        injector = Guice.createInjector(new Module() {
            @Override
            public void configure(Binder binder) {
                binder.bind(MockFactory.class).to(MockFactoryImpl.class).in(Singleton.class);
                binder.bind(ServerService.class).to(ServerServiceImpl.class).in(Singleton.class);

                // parsers
                binder.bind(BlueprintParserService.class).to(SnowcrashBlueprintParserServiceImpl.class);
                binder.bind(AstParserService.class).to(JsonAstParserServiceImpl.class);
            }
        });

        injector.injectMembers(service);

        serverService = injector.getInstance(ServerService.class);
    }

    @Test
    public void testReadBlueprintAndStartMock() throws Exception {
        // blueprint file
        final File jsonAst = new File(EndToEndTest.class.getResource("/api1.json").getPath());

        // start server from blueprint
        final ExecutionInstance exec = service.execute(jsonAst, MockFactory.BlueprintFormat.AST_JSON);

        // assert
        Assert.assertNotNull(exec);
        Assert.assertNotNull(exec.getEndpoints());
        Assert.assertEquals(1, exec.getEndpoints().length);

        // test endpoint
        RestAssured
                .get("http://localhost:" + exec.getPort() + "/message")
                .then()
                .assertThat()
                .statusCode(is(StatusCodes.OK))
                .and()
                .body(equalTo("Hello World!\n"));

        // stop server
        serverService.stop(exec);
    }
}
