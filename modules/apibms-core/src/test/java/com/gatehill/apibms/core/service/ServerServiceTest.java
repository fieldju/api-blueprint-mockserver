package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.model.*;
import com.gatehill.apibms.core.service.ServerServiceImpl;
import com.jayway.restassured.RestAssured;
import io.undertow.util.Headers;
import io.undertow.util.StatusCodes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * Created by pete on 15/02/2014.
 */
public class ServerServiceTest {

    private ServerServiceImpl service;

    @Before
    public void before() {
        service = new ServerServiceImpl();
    }

    @Test
    public void testStartStop() throws Exception {
        final MockDefinition mock = new MockDefinition();

        final ResourceDefinition endpoint = new ResourceDefinition("My API");
        mock.addEndpoint(endpoint);

        final RequestDefinition request = new RequestDefinition();
        request.setVerb("GET");

        endpoint.getRequests().add(request);
        endpoint.setUrl("/message");

        final ResponseDefinition response = new ResponseDefinition();
        endpoint.getResponses().put(StatusCodes.OK, response);

        response.getHeaders().put(Headers.CONTENT_TYPE_STRING, "text/plain");
        response.setCode(StatusCodes.OK);
        response.setBody("Hello World!\n");

        // call
        final MockServer actual = service.start(mock);

        // assert
        Assert.assertNotNull(actual);

        // call endpoint
        RestAssured
                .get("http://localhost:" + actual.getPort() + "/message")
                .then()
                .assertThat()
                .statusCode(is(StatusCodes.OK))
                .and()
                .body(equalTo("Hello World!\n"));

        // stop
        service.stop(actual);
    }
}
