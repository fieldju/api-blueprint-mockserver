package com.gatehill.apibms.core.service;

import com.gatehill.apibms.core.model.MockDefinition;
import com.gatehill.apibms.core.model.RequestDefinition;
import com.gatehill.apibms.core.model.ResourceDefinition;
import com.gatehill.apibms.core.model.ResponseDefinition;
import com.gatehill.apibms.core.service.mockfactory.MarkdownMockFactoryImpl;
import com.gatehill.apibms.core.service.mockfactory.MockFactory;
import io.undertow.util.StatusCodes;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.InputStream;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

/**
 * Created by pete on 15/02/2014.
 */
public class MarkdownMockFactoryTest {

    @InjectMocks
    private MarkdownMockFactoryImpl factory;

    @Mock
    private ParsingService parsingService;

    @Before
    public void before() {
        factory = new MarkdownMockFactoryImpl();

        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateMock() throws Exception {
        // mock behaviour
        when(parsingService.parseMarkdownFile(any(File.class)))
                .thenReturn("html");

        when(parsingService.toXhtmlDocument(anyString()))
                .thenReturn(loadXMLFromString(MarkdownMockFactoryTest.class.getResourceAsStream("/api1.xhtml")));

        // test data
        final File blueprint = new File(MarkdownMockFactoryTest.class.getResource("/api1.md").getPath());

        // call
        final MockDefinition actual = factory.createMock(blueprint, MockFactory.BlueprintFormat.MARKDOWN);

        // assert
        Assert.assertNotNull(actual);
        Assert.assertEquals("My API", actual.getName());
        Assert.assertEquals(1, actual.getEndpoints().size());

        final ResourceDefinition endpoint1 = actual.getEndpoints().get(0);
        Assert.assertNotNull(endpoint1);
        Assert.assertEquals("/message", endpoint1.getUrl());

        Assert.assertEquals(1, endpoint1.getRequests().size());
        final RequestDefinition request = endpoint1.getRequests().get(0);
        Assert.assertEquals("GET", request.getVerb());

        Assert.assertEquals(1, endpoint1.getResponses().size());
        final ResponseDefinition response = endpoint1.getResponses().get(StatusCodes.OK);

        // FIXME trailing \n is stripped off
        Assert.assertEquals("Hello World!", response.getBody());

        // verify behaviour
        verify(parsingService, times(1)).parseMarkdownFile(any(File.class));
        verify(parsingService, times(1)).toXhtmlDocument(anyString());
        verifyNoMoreInteractions(parsingService);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testCreateMock_UnsupportedFormat() throws Exception {
        // test data
        final File blueprint = new File(MarkdownMockFactoryTest.class.getResource("/api1.json").getPath());

        // call
        factory.createMock(blueprint, MockFactory.BlueprintFormat.AST_JSON);

        // assert
        Assert.fail(UnsupportedOperationException.class + " should have been thrown");

        // verify behaviour
        verifyNoMoreInteractions(parsingService);
    }

    /**
     * Load XHTML document.
     *
     * @param xml
     * @return
     * @throws Exception
     */
    private Document loadXMLFromString(InputStream xml) throws Exception {
        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        // performance - see http://jdevel.wordpress.com/2011/03/28/java-documentbuilder-xml-parsing-is-very-slow/
        factory.setNamespaceAware(false);
        factory.setValidating(false);

        factory.setFeature("http://xml.org/sax/features/namespaces", false);
        factory.setFeature("http://xml.org/sax/features/validation", false);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

        final DocumentBuilder builder = factory.newDocumentBuilder();

        return builder.parse(xml);
    }
}
